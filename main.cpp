#include "deck.h"
#include "card.h"

void printHand(vector<Card> hand);

int main(){

	vector<Card> playerone, playertwo;

	Deck deck;

	deck.shuffle();

	for (int i=0; i<6; i++){
		playerone.push_back(deck.dealCard());
		playertwo.push_back(deck.dealCard());
	}

	printHand(playerone);
	cout << endl;
	printHand(playertwo);

	std::sort(playerone.begin(), playerone.end());
	std::sort(playertwo.begin(), playertwo.end());

	cout << endl;
	cout << "after sort" << endl;
	printHand(playerone);
	cout << endl;
	printHand(playertwo);	
    cout << endl;

    Card card1('H', 3);
    Card card2('D', 8);

    cout << "checking cards";

    if (card1 > card2){
    	cout << "card1 is greater";
    }
    else{
    	cout << "card 2 is greater";
    }


	return 0;
}

void printHand(vector<Card> hand){

	for (vector<Card>::iterator vit = hand.begin(); vit != hand.end(); ++vit){
		vit->printCard();
		cout << " ## ";
	}
}