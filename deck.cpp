#include <algorithm>
#include <cstdlib>

#include "card.h"
#include "deck.h"

Deck::Deck(){

	for (int i = 1; i <= 13; i++){

	    Card anewCard('H', i);
		deck.push_back(anewCard);

		Card bnewCard('D', i);
		deck.push_back(bnewCard);

		Card cnewCard('C', i);
		deck.push_back(cnewCard);

		Card dnewCard('S', i);
		deck.push_back(dnewCard);
	}
}

void Deck::shuffle(){
	std::random_shuffle(deck.begin(), deck.end());
}

Card Deck::dealCard(){
	Card c = deck.back();
	deck.pop_back();
	return c;
}   