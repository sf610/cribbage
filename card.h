#ifndef CARD_H
#define CARD_H

#include <iostream>

using namespace std;

class Card{

private:
	char suit;
	int value;
public:
	Card(char, int);
    void printCard();
    bool operator<(const Card & c);
    bool operator>(const Card & c);
    bool operator==(const Card & c);
    char getsuit();
    int getvalue();
    int getpoints();
};

#endif