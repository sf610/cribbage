#include "card.h"

#include <vector>	

using namespace std;

class Hand{
public:
	Hand(vector<Card>);
	void AddCard(Card);
    Card ThrowCard();
    int HandCount();
    void MoveCard(Card);
    Card GetLowestCard();
    Card GetHighestCard();
    

private:
	vector<Card> currentHand;
	vector<Card> playingHand;
};