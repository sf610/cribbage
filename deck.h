#ifndef DECK_H
#define DECK_H

#include <iostream>
#include <vector>
#include <algorithm>

#include "card.h"

using namespace std;

class Deck{

public:
     Deck();
     Card dealCard();
     void shuffle();
private:
    vector<Card> deck;
};

#endif