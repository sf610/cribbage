#include "card.h"

Card::Card(char _s, int _v){
    suit = _s;
    value = _v;
}

void Card::printCard(){

    switch (value){
        case 11:
            cout << "J";
            break;
        case 12:
            cout << "Q";
            break;
        case 13:
            cout << "K";
            break;
        default:
            cout << value;
            break;
    }

    cout << "->" << suit;
}

bool Card::operator<(const Card &a){
    return this->value < a.value;
    }

bool Card::operator>(const Card &a){
return this->value > a.value;
}

bool Card::operator==(const Card &a){
    return (this->value == a.value) && (this->suit == a.suit);
}

char Card::getsuit(){return suit;}
int Card::getvalue(){return value;}

int Card::getpoints(){
    return (value > 10)? 10: value;
}